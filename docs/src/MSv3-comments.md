# MSv3-comments

1) The MeasurementSet is not an entity but just a concept or convention. Usually this is implemented as a directory containing table and/or column files. This leads to problems when very different ways of storing/transporting measurement sets are being used. If one would use a DB storage manager or the ADIOS2 or HDF5 storage manager, the concept of directories does not exist. Moreover, given one file from a MS in isolation, there is no way to identify to which MS it belongs. Without a MS entity it is also not possible to firmly attach provenance information to an MS. Currently this is emulated by kind of mis-using the Observation and History tables, but what happens when the Observation table is being detached from a MS? Since there is no MS identifier, there is no way to identify to which MS that Observation table originally belonged to. The unique entity identifier issue is already being addressed and discussed within the SKA.

1) The MS Table is not defined/modeled. Also tables are not explicit entities in MSv3. Similar problems as above, but it is getting really messy when different storage managers are being used for individual columns of a single table, since in the standard implementation this leads to individual files for each column and they are kept together just by directory structure/file name.

1) MSv3 is just a lowest common denominator. For real world instruments many additional tables are required and it is also very likely that additional provenance and other information is required. Again in most cases the Observation table is (mis-)used, but none of the entries are explicitly identifyable to relate to one of the entities of an MS. In order to facilitate this the UML model proposes a key-value store kind of class (called MsMetaData), which allows to explicitly link entries to any uniquely identifiable entity in a MS.

1) The time_extra_precision column has the unit [s], which can't be correct, else it would not give any extra precision. This is anyway not the correct way to model this and should be done like in the ASDM as a proper type of ArrayTime, which provides not just the required precision, but also the access methods.

1) In the main table the MSversion column should be in the MS entity, not in a table at all.

1) In Main table DATA, VIDEO and LAG_DATA are optional, but FLAG and WEIGTH are not. Seems inconcistent

1) The Flag_cmd table has only time and interval as keys. Does that mean that MSv3 can't support local Antenna based flagging or spectral window flagging? ASDM has those relations as well.

1) The Frequency_offset table does not have an Antenna3 attribute. Not sure if this is required for phase closure cases.

1) In the pointing table the pointing_model_id is not a key.

1) The Polarization table does not have an ID and seems very skinny (incomplete?)
